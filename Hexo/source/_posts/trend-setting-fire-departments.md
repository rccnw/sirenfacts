---
title: Trend setting fire departments
date: 2016-01-17 20:25:14
tags: ['Fire', 'Culture']
---

Change from within is not impossible. Some major fire departments have taken the initiative to restrict siren usage.
Admittedly these policy changes were not in consideration of the community needs, but rather an effort to minimize accidents by emergency vehicles.

[St. Louis Fire Department: "On The Quiet" Policy A Success](http://www.firehouse.com/news/10544837/runs-up-accidents-down-on-the-quiet-policy-a-success)

[Firefighters in Queens Won’t Rush to All Calls](http://www.nytimes.com/2010/10/04/nyregion/04fire.html?_r=0)