---
title: Study demonstrates siren ineffectiveness
date: 2016-01-17 18:17:14

tags: ['Research', 'Technology']
---

This video is produced by a New Jersey hospital consortium with a fleet of over 100 ambulances. They were sick and tired of the accidents and lawsuuits, so they produced this video to dispel myths about siren effectiveness.

[Driving Responsibly-The Truth about Sirens](https://vimeo.com/64177381)






