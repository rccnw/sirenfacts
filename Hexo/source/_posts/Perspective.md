---
title: Perspective of a Law Enforcement Veteran

tags: ['Police', 'Culture', 'Opinion']
---

For a great introduction to the siren problem, read this important article by a veteran law enforcement officer.

[Emergency Fire and Police Sirens; The Loud Noise Bully of the Twenty-First Century](http://hubpages.com/politics/Emergency-Vehicle-Sirens-The-Noise-Bully-of-the-Twenty-First-Century)
