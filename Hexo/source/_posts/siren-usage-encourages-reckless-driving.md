---
title: Siren usage encourages reckless driving
date: 2016-01-17 20:45:55
tags: ['Technology', 'Safety', 'Culture' ]
---


[Sirencide: The Mental State of Reckless Drivers](http://www.emsconedonline.com/pdfs/sirencide.pdf)

[YouTube Fire Truck Crash Complication](https://www.youtube.com/watch?v=xz4pQh3DJdE)