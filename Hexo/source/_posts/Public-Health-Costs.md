---
title: Public Health Costs
date: 2016-01-19
tags: ['Medical', 'Harm', 'Environment' ]
---
Environmental noise takes a toll on human beings. This is established fact. We can see statistically in public health data irrefutable evidence of this. 

Sirens contribute significantly to negative health outcomes in the affected communities. File under sad irony.

## Stroke and heart attack

[Noise Pollution: The Sound Behind Heart Effects](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2072857/)

[...or google it!](https://www.google.com/search?q=noise+heart+attack&oq=noise+heart+atacck&aqs=chrome.1.69i57j0l5.7175j0j7&sourceid=chrome&es_sm=122&ie=UTF-8)

## Sleep impairment

[Environmental noise and sleep disturbances](http://www.sciencedirect.com/science/article/pii/S1984006314000601)
[...or google it!](https://www.google.com/search?q=sleep+impairment+noise&oq=sleep+impairment+noise&aqs=chrome..69i57.12887j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#q=sleep+noise+health)

## Chronic stress  

[Acute and chronic endocrine effects of noise](https://www.wind-watch.org/documents/acute-and-chronic-endocrine-effects-of-noise/)

[Fire & Emergency Service; Hearing Conservation Program Manual - page 15](https://books.google.com/books?id=B1oDFxJ2Kq8C&pg=PA15&lpg=PA15&dq=siren+noise+hypertension&source=bl&ots=G699dNseBj&sig=_jYd5013xcwk9rbv1VoCwRowuRw&hl=en&sa=X&ved=0ahUKEwjf__OMirfKAhWEthQKHfhfACIQ6AEIHTAA#v=onepage&q=siren%20noise%20hypertension&f=false)
* note - this book is a free download *

[...or google it!](https://www.google.com/search?q=sleep+impairment+noise&oq=sleep+impairment+noise&aqs=chrome..69i57.12887j0j4&sourceid=chrome&es_sm=122&ie=UTF-8#q=chronic+noise+stress)