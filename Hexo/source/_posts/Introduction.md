---
title: Introduction
date: 
tags: ['Opinion']
---


# Emergency Sirens cause harm to communities, without evidence of value.

*“Red-lights-and-sirens never saved anyone’s life in the history of the world.”*

- An American Ambulance Association Official

## The problem

Sirens are overused.  Although taken at face value as being a vital component of emergency services operations, the truth is the very opposite.  Sirens are an adornment, a cultural artifact, uselessly ceremonial and little else. In the rare cases they contribute to saving time, there are always other ways that time savings could have been accomplished by smarter deployment of appropriate resources.   

Regardless of any disputed value, the harm they do both to communities and to their operators is real and  documented. These harms demand an accounting and recognition.

Sirens diminish our quality of life, sometimes to an extreme degree. Loud noise is a proven cause of a vast number of public health issues. It is well proven that lifespans are shortened by chronic environmental sound problems.

Sirens invite accidents, emergency vehicle drivers are three times more likely to be involved in an accident when responding with their lights and sirens operating.  Vehicular accidents to and from emergency scenes are the second leading cause of deaths among on-duty emergency responders.

Sirens are an antiquated and inappropriate technology that serve the public interest poorly.

## The solution

An objective examination of actual benefits versus actual harm is well justified and long overdue, as well as a review of other more modern and thoughtful solutions to the problem of rapid emergency response.

If the sirens don't meaningfully make a difference in the final outcomes, then stop using them. Were lives actually saved that wouldn't have been?  In any cases where they might have helped, what other ways could have producedd the same outcome? 

These are questions that have answers, why isn't anybody asking them?


## Discussion

### Sirens 24×7

Emergency sirens are heard everywhere in the urban environment at all hours of the day and night, day in and day out.  They scream ‘warning!’ and ‘make way!’ to everyone within earshot, which is quite a lot of people as it turns out, and most of them have no need whatsoever to hear this noise.  Most people who live on major arterial streets or in the urban core suffer day and night from the onslaught of sirens.  We are robbed of our right to rest and repose in our homes after the toil of life.


## Professionals debate siren usage

Emergency services researchers and professionals who have considered this topic with rigor have arrived at conclusions that fly in the face of common practice. This is not news, its been going on in professional journals and forums for decades.

There appears to be limited awareness of professional practices in other cities, and of the professional debate over siren usage.  The info on this website is just a sampling, there are many more resources beyond what is represented here.


## Where is the oversight?

It appears that some local fire/ems/police agencies are either unaware of these guidelines or have chosen to ignore them.  That should raise questions among those charged with oversight of these agencies that don’t keep up with evolving professional standards.  Where else is the regulation left to the regulated?

## Summary

- The use of sirens by emergency vehicles conveys benefit only in a very narrow and limited set of circumstances, and even in those situations other choices often would be better or sufficient when measured by ‘lives saved’.

- The use of sirens has negative consequences for many people, from deadly results when accidents occur to the maddening noise invading our lives everywhere we go. If you could put a price tag on this harm, it would be significant.


In this blog, I provide information I have collected which addresses these topics. Some of it may surprise you, and at the very least I hope it will cause you to likewise wonder about sirens and to support the position that a review of siren practices is long overdue.

For a great introduction to the siren problem, read this important article by a veteran law enforcement officer.

[Emergency Fire and Police Sirens; The Loud Noise Bully of the Twenty-First Century](https://web.archive.org/web/20150913035618/http://kryptowrite.hubpages.com/hub/Emergency-Vehicle-Sirens-The-Noise-Bully-of-the-Twenty-First-Century)


thanks for reading.

*For additional information you may write to:*

info @ sirenfacts.com

*remove the spaces, this is to prevent spambots from harvesting the email*





### Naysayer?
If you take issue with the material here, by all means share any links to information from professional sources such as journals, published authors, or government documents. 

Everybody is entitled to an opinion, but I insist you read this book before offering your own, however heartfelt and passionate you may be. There is a very very good chance you are dead wrong. This Nobel prize winner has your number:

It turns out it can be very challenging to 'think your way out of a paper bag'.  Don't be so sure!
[Thinking, Fast and Slow](https://en.wikipedia.org/wiki/Thinking,_Fast_and_Slow)


