---
title: Insufficient Sleep Is a Public Health Problem
date: 2016-01-22 08:33:20
tags: ['Medical', 'Harm', 'Environment' ]
---


Insufficient sleep is associated with a number of chronic diseases and conditions—such as diabetes, cardiovascular disease, obesity, and depression—which threaten our nation’s health. Aside from these insufficient sleep is also responsible for motor vehicle and machinery-related crashes, causing substantial injury and disability each year.


[CDC : Sleep and Sleep Disorders](http://www.cdc.gov/sleep/index.html)

[CDC : Insufficient Sleep Is a Public Health Problem](http://www.cdc.gov/features/dssleep/)

[Fragmented sleep in older adults may lead to severe cerebral arteriosclerosis](http://www.healio.com/cardiology/stroke/news/online/%7B60b68522-6931-458a-a0c0-bf8d9a6a8676%7D/fragmented-sleep-in-older-adults-may-lead-to-severe-cerebral-arteriosclerosis)

[Poor Sleep May Mean Higher Stroke Risk](http://www.webmd.com/healthy-aging/news/20160114/for-seniors-poor-sleep-may-mean-higher-stroke-risk-study-suggests)

[Sleep Problems in the Transportation Industry](http://www.huffingtonpost.com/dr-michael-j-breus/sleep-transportation_b_4419704.html)

[Sleep Deprivation Raises Diabetes Risk](http://www.huffingtonpost.com/dr-michael-j-breus/sleep-diabetes_b_4639612.html)

[Insomnia Increases Suicide Risk](http://www.huffingtonpost.com/dr-michael-j-breus/depression-and-insomnia_b_3021852.html)