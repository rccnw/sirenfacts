---
title: Patient stress during transport
date: 2016-02-02 18:17:14

tags: ['Medical', 'Harm']
---

'A fast transport, especially in the case of a cardiac emergency, can do more harm than good.  Siren noise can induce physiological stress.'
[Effects of emergency transportation - Additional stress for the Patient?](http://www.witzel-chirurgie.de/PDFs/JJAAM.pdf)


'Emergency transport by an ambulance can cause considerable psychical and physical stress for patients.'
[The influence of the mode of emergency ambulance transportation on the emergency patient's outcome.](http://www.ncbi.nlm.nih.gov/pubmed/10461554)

'During a typical emergency ambulance transport, patients are subjected to stress.'
[Effects of Emergency Ambulance Transportation on Heart Rate, BloodPressure, Corticotropin, and Cortisol](http://goo.gl/i7zgkw)

'The stress of high speed ambulance transport particularly in patients with acute cardiac disease may result in additional morbidity.'
[The influence of the mode of emergency ambulance transportation on the emergency patient's outcome.](http://www.ncbi.nlm.nih.gov/pubmed/10461554)

