---
title: Sirens vs. reason
date: 2016-01-17 21:27:27
tags: ['Technology', 'Safety', 'Culture' ]
---

"Sirencide" describes the emotional reaction of emergency vehicle operators when they begin to feel a sense of power and urgency that blocks out reason and prudence. It often leads to reckless driving and increases as siren use increases.

Research has shown that the average response time saved by lights and siren use is 43 seconds. According to Dr. Jeff Clawson, President of Medical Priority Dispatch and founding father of the Emergency Medical Dispatch System, approximately 95 percent of all 911 EMS calls aren't life-threatening or even potentially life threatening. Of the rest, less than 3 percent turn out to be life-threatening or potentially life-threatening.

[Lights, Sirens...Action!](http://www.firehouse.com/article/10503482/lights-sirensaction)